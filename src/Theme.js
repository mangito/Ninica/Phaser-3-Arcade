const PressStart2P = "'Press Start 2P'";

export const Banner = {
	Text: "#ffffff",
	Background: [
		"#ffffff",
		"#0000ff",
		"#00ff00",
		"#ff0000",
		"#000000",
	],
};

const baseShadow = {
	offsetX: 2,
	offsetY: 2,
	color: "#ff0",
	blur: 2,
	stroke: true,
	fill: true,
};

export const TextStyle = {
	progressBar: {
		fontFamily: PressStart2P,
		fontSize: 10,
		fill: "#fff",
		stroke: "#000",
		strokeThickness: 2,
	},
	home: {
		title: {
			fontFamily: PressStart2P,
			fontSize: 50,
			align: "center",
			fill: "#fff",
			stroke: "#fff",
			strokeThickness: 2,
			shadow: baseShadow,
		},
		subTitle: {
			fontFamily: PressStart2P,
			fontSize: 20,
			align: "center",
			fill: "#ff0",
			stroke: "#fff",
			strokeThickness: 2,
		},
		menu: {
			fontFamily: PressStart2P,
			fontSize: 30,
			align: "center",
			fill: "#ff0",
			stroke: "#000",
			strokeThickness: 2,
			shadow: {
				...baseShadow,
				color: "#fff",
				offsetX: 0,
				offsetY: 0,
			},
		},
	},
	selectLevel: {
		title: {
			fontFamily: PressStart2P,
			fontSize: 50,
			align: "center",
			fill: "#fff",
			shadow: baseShadow,
		},
		levels: {
			fontFamily: PressStart2P,
			fontSize: 30,
			align: "center",
			fill: "#fff",
		},
		back: {
			fontFamily: PressStart2P,
			fontSize: 20,
			align: "center",
			fill: "#fff",
			shadow: {
				...baseShadow,
				offsetX: 0,
				offsetY: 0,
				color: "#f00",
				blur: 4,
			},
		},
	},
	game: {
		score: {
			fontFamily: PressStart2P,
			fontSize: 20,
			fill: "#fff",
			stroke: "#0f0",
			strokeThickness: 2,
		},
	},
	pause: {
		title: {
			fontFamily: PressStart2P,
			fontSize: 50,
			align: "center",
			fill: "#fff",
			shadow: baseShadow,
		},
		continue: {
			fontFamily: PressStart2P,
			fontSize: 40,
			align: "center",
			fill: "#0f0",
			shadow: {
				...baseShadow,
				offsetX: 0,
				offsetY: 0,
				blur: 10,
				color: "#f00",
			},
		},
	},
	gameOver: {
		title: {
			fontFamily: PressStart2P,
			fontSize: 50,
			align: "center",
			fill: "#f00",
			shadow: {
				...baseShadow,
				color: "#a60000",
			},
		},
		levelName: {
			fontFamily: PressStart2P,
			fontSize: 35,
			align: "center",
			fill: "#fff",
		},
		score: {
			fontFamily: PressStart2P,
			fontSize: 40,
			align: "center",
			fill: "#fff",
		},
		bestScore: {
			fontFamily: PressStart2P,
			fontSize: 20,
			align: "center",
			fill: "#fff",
		},
		playAgain: {
			fontFamily: PressStart2P,
			fontSize: 30,
			align: "center",
			fill: "#0f0",
		},
		back: {
			fontFamily: PressStart2P,
			fontSize: 30,
			align: "center",
			fill: "#f00",
		},
	},
	settings: { // GameSettings
		title: {
			fontFamily: PressStart2P,
			fontSize: 50,
			align: "center",
			fill: "#ff0",
			shadow: {
				...baseShadow,
				color: "#ff0",
			},
		},
		menu: {
			fontFamily: PressStart2P,
			fontSize: 30,
			align: "center",
			fill: "#fff",
			stroke: "#000",
			strokeThickness: 2,
			shadow: {
				...baseShadow,
				color: "#fff",
				offsetX: 0,
				offsetY: 0,
				blur: 1,
			},
		},
		baseColor: {
			true: "#00ff00", // On
			false: "#ff0000", // Off
			disabled: "#6e6e6e", // Disabled
		},
		back: {
			fontFamily: PressStart2P,
			fontSize: 20,
			align: "center",
			fill: "#fff",
			shadow: {
				offsetX: 0,
				offsetY: 0,
				color: "#f00",
				blur: 4,
				stroke: true,
				fill: true,
			},
		},
	},
	leaderboard: {
		title: {
			fontFamily: PressStart2P,
			fontSize: 50,
			align: "center",
			fill: "#ff0",
			shadow: {
				...baseShadow,
				color: "#ff0",
			},
		},
		leaderboard: {
			fontFamily: PressStart2P,
			fontSize: 20,
			align: "center",
			fill: "#fff",
		},
		noLeaderboard: {
			fontFamily: PressStart2P,
			fontSize: 30,
			align: "center",
			fill: "#fff",
		},
		back: {
			fontFamily: PressStart2P,
			fontSize: 20,
			align: "center",
			fill: "#fff",
			shadow: {
				offsetX: 0,
				offsetY: 0,
				color: "#f00",
				blur: 4,
				stroke: true,
				fill: true,
			},
		},
	},
	levelsNamesBaseColors: {
		easy: "#0f0",
		normal: "#ff0",
		hard: "#f00",
	}
};
