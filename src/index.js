import Phaser from "phaser";

// import "./CSS/Reset.css";

import GlobalConfigs from "./Config/Configs";

import favicon from "./Assets/Favicon/favicon.png";

// Configs/Infos
import packageJson from "../package.json";
import { Banner } from "./Theme";

// Scenes
import Preload from "./Scenes/Preload";
import Home from "./Scenes/Home";
import GameSettings from "./Scenes/GameSettings";
import SelectLevel from "./Scenes/SelectLevel";
import Leaderboard from "./Scenes/Leaderboard";
import Game from "./Scenes/Game";
import Pause from "./Scenes/Pause";
import GameOver from "./Scenes/GameOver";

window.addEventListener("load", () => {
	const config = {
		title: "HomoGame - Phaser 3 Arcade",
		url: packageJson.homepage,
		version: packageJson.version,
		banner: {
			text: Banner.Text,
			background: Banner.Background,
			hidePhaser: false,
		},
		// Game
		parent: "GameContainer",
		type: Phaser.AUTO,
		width: GlobalConfigs.screen.width,
		height: GlobalConfigs.screen.height,
		backgroundColor: "#000000",
		scale: {
			mode: Phaser.Scale.FIT,
			autoCenter: Phaser.Scale.CENTER_BOTH,
			width: GlobalConfigs.screen.width,
			height: GlobalConfigs.screen.height,
		},
		transparent: false,
		antialias: true,
		pixelArt: false,
		roundPixels: true,
		input: {
			gamepad: true,
		},
		physics: {
			default: "arcade",
			arcade: {
				debug: GlobalConfigs.debug,
				gravity: { x: 0, y: 300, },
			},
		},
		scene: [
			Preload,
			Home, GameSettings, SelectLevel, Leaderboard,
			Game, Pause, GameOver,
		]
	}

	const game = new Phaser.Game(config);

	document.getElementById("favicon").setAttribute("href", favicon);

	// Landscape
	if (window.innerWidth < window.innerHeight) {
		game.scale.lockOrientation(Phaser.Scale.LANDSCAPE);
	}
});
