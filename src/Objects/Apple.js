import Configs from "../Config/Configs";
import { randomNumber } from "../Utils/Utils";

export class Apple extends Phaser.Physics.Arcade.Sprite {
	constructor(scene, x = 0, y = 0, isRandom = true) {
		super(scene, -100, -100, "Apple");

		this.alive = true;
		this.isLightActive = false;
		this.isRandom = isRandom;

		this.setVisible(false);
		this.setAlpha(0);
		if ((x && y) || !isRandom) this.setPosition(x, y);

		const removeDelay = (isRandom ? randomNumber(15, 25) : 20) * 1000;
		this.killTimer = this.scene.time.delayedCall(removeDelay, () => this.kill());

		this.particles = this.scene.add.particles("Apple")
			.createEmitter({
				x: -100,
				y: -100,
				frequency: -1,
				quantity: 100,
				scale: { start: 0.5, end: 0 },
				alpha: { start: 0.5, end: 0 },
				speed: { min: 50, max: 150 },
				lifespan: 1000,
				on: true,
			});

		this.scene.tweens.add({
			targets: this,
			ease: "Linear",
			duration: 500,
			delay: 50,
			scale: { from: 0, to: 1, },
			alpha: { from: 0, to: 1, },
			onStart: () => this.setVisible(true),
			onUpdate: () => this.refreshBody(),
		});
	}

	updatePosition() {
		if (!this.alive, !this.scene) return;
		const { width, height, middleWidth, middleHeight } = Configs.screen;
		const marginSize = this.width / 2;
		this.setRandomPosition(marginSize, marginSize, width - this.width, height - this.width);
		this.refreshBody();
	}

	activateLights() {
		this.isLightActive = true;
		this.shadowTimer = this.scene.time.delayedCall(1000, () => this.setPipeline("Light2D"));
	}

	explode() {
		const { x, y, alive, scene } = this;
		if (!alive || !scene) return;

		this.particles.setPosition(x, y);
		this.particles.explode();

		this.kill();
	}

	kill() {
		const { x, y, alive, scene, killTimer, shadowTimer } = this;
		if (!alive, !scene) return;
		this.alive = false;

		this.disableBody();
		killTimer.destroy();
		if (shadowTimer) shadowTimer.destroy();
		this.setAlpha(0.5);

		if (this.isRandom) {
			// Create new Duke
			const duke = this.scene.dukeGroup.get(x, y);
			duke.refreshBody();
			if (this.isLightActive) duke.activateLights();
		}

		scene.tweens.add({
			targets: [this],
			delay: 50,
			duration: 500,
			ease: "Linear",
			scale: { from: 1, to: 0, },
			onComplete: () => this.completeDestroy(),
		});
	}

	completeDestroy() {
		this.particles.remove();
		this.destroy();
	}
}

export default class AppleGroup extends Phaser.Physics.Arcade.StaticGroup {
	constructor(world, scene) {
		const config = {
			classType: Apple,
			collideWorldBounds: true,
		};
		super(world, scene, config);

		this.isLightActive = false;

		this.createNewApple();

		this.scene.time.addEvent({
			delay: 5 * 1000, // 5 seconds
			callback: this.createNewApple,
			callbackScope: this,
			loop: true,
		});
	}

	createNewApple(x = 0, y = 0, isRandom = true) {
		if (!this.scene || !this.scene.player.alive) return;
		const newApple = this.get(x, y, isRandom);
		if (this.isLightActive) newApple.activateLights();
		if (isRandom) newApple.updatePosition();
	}

	activateLights() {
		this.isLightActive = true;
		this.children.each(child => {
			child.isLightActive = true;
			child.activateLights();
		});
	}
}
