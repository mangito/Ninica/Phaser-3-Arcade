import Configs from "../Config/Configs";

export class Duke extends Phaser.Physics.Arcade.Sprite {
	constructor(scene, x = 0, y = 0) {
		super(scene, -100, -100, "Duke");
		this.setScale(0);

		this.alive = true;

		this.scene.time.delayedCall(10, () => { this.setCircle(this.width / 2); });

		this.scene.tweens.add({
			targets: [this],
			delay: 1500,
			duration: 1000,
			ease: "Back",
			scale: { from: 0, to: 0.5, },
			onStart: () => this.setPosition(x, y),
			onUpdate: () => this.refreshBody(),
		});

		this.particles = this.scene.add.particles("Dead")
			.createEmitter({
				x: 0,
				y: 0,
				frequency: -1,
				quantity: 250,
				scale: { start: 0.3, end: 0 },
				alpha: { start: 0.5, end: 0 },
				speed: { min: 50, max: 250 },
				lifespan: Configs.timers.restart / 2,
			});
	}

	activateLights() {
		this.setPipeline("Light2D");
	}

	kill() {
		// this.scene.physics.world.disableBody(this);
		// this.body.setEnable(false);
		if (!this.alive) return;
		this.alive = false;

		this.disableBody();

		this.particles.setPosition(this.x, this.y);
		this.particles.explode();

		this.setAlpha(0.5);

		this.scene.tweens.add({
			targets: this,
			ease: "Linear",
			duration: Configs.timers.restart,
			scale: { from: 0.75, to: 0, },
			alpha: { from: 1, to: 0, },
			onComplete: () => this.completeDestroy(),
		});
	}

	completeDestroy() {
		this.particles.remove();
		this.destroy();
	}
}

export default class DukeGroup extends Phaser.Physics.Arcade.StaticGroup {
	constructor(world, scene) {
		const config = {
			classType: Duke,
			collideWorldBounds: true,
		};
		super(world, scene, config);

		this.isLightActive = false;
	}

	activateLights() {
		this.isLightActive = true;
		this.children.each(child => {
			child.activateLights();
		});
	}
}
