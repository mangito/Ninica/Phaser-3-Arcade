import Configs from "../Config/Configs";
import Settings from "../State/Settings";

import { TextStyle } from "../Theme";

import ModalBG from "../Components/ModalBG";
import TextButton from "../Components/TextButton";

export default class Pause extends Phaser.Scene {
	constructor() {
		super({ key: "Pause", });
	}

	init() {
		this.Settings = new Settings();
	}

	create() {
		const { width, height, middleWidth, middleHeight } = Configs.screen;

		{ // Inputs
			const keysPause = this.input.keyboard.addKeys("P, ESC");

			keysPause.P.removeAllListeners();
			keysPause.ESC.removeAllListeners();

			keysPause.P.on("down", this.resumeGame, this);
			keysPause.ESC.on("down", this.resumeGame, this);
		}

		const modalBG = new ModalBG(this);
		modalBG.addTitle("pause");

		const continueBTN = new TextButton(this, middleWidth, middleHeight, this.Settings.output.continue, TextStyle.pause.continue,
			() => this.resumeGame());
	}

	resumeGame() {
		this.scene.resume("Game");
		this.scene.stop();
	}
}
