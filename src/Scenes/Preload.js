import ProgressBar from "../Components/ProgressBar";

// ---- Assets
import Apple from "../Assets/Sprites/Apple.png";
import Arrow from "../Assets/Sprites/Arrow.png";
import Dead from "../Assets/Sprites/Dead.png";
import Duke from "../Assets/Sprites/Duke/Duke.png";
import Platform from "../Assets/Sprites/Platform.png";
import Player from "../Assets/Sprites/Player.png";
import PowerUp from "../Assets/Sprites/PowerUp.png";

export default class Preload extends Phaser.Scene {
	constructor() {
		super({ key: "Preload", active: true, });
	}

	preload() {
		const progressBar = new ProgressBar(this);

		this.importSprites();
		this.importSounds();

		this.load.on("progress", (p) => progressBar.updateBar(p));
		this.load.on("fileprogress", (f) => progressBar.fileLoad(f));
		this.load.on("complete", () => progressBar.complete());
	}

	importSprites() {
		this.load.image("Apple", Apple);
		this.load.image("Arrow", Arrow);
		this.load.image("Dead", Dead);
		this.load.image("Duke", Duke);
		this.load.image("Platform", Platform);
		this.load.image("Player", Player);
		this.load.image("PowerUp", PowerUp);
	}

	importSounds() { }

	create() {
		this.scene.start("Home");
		// this.scene.start("Game");
	}
}
