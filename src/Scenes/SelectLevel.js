import Configs from "../Config/Configs";
import Settings from "../State/Settings";

import EventManager from "../State/EventManager";

import { TextStyle } from "../Theme";

import ModalBG from "../Components/ModalBG";
import TextButton from "../Components/TextButton";

export default class SelectLevel extends Phaser.Scene {
	constructor() {
		super({ key: "SelectLevel", });
	}

	init() {
		this.Settings = new Settings();
	}

	create() {
		const { width, height, middleWidth, middleHeight } = Configs.screen;

		{ // Inputs
			const exitKey = this.input.keyboard.addKey("ESC");
			exitKey.removeAllListeners();
			exitKey.on("down", this.returnHome, this);
		}

		const modalBG = new ModalBG(this);
		modalBG.addTitle("selectLevel");

		{ // Easy Game Button
			const easyBTN = new TextButton(this, middleWidth, middleHeight - 100, this.Settings.output.easy, TextStyle.selectLevel.levels,
				() => this.startGame("easy"))
				.setColor("#0f0");
		}

		{ // Normal Game Button
			const normalBTN = new TextButton(this, middleWidth, middleHeight, this.Settings.output.normal, TextStyle.selectLevel.levels,
				() => this.startGame("normal"))
				.setColor("#ff0");
		}

		{ // Hard Game Button
			const hardBTN = new TextButton(this, middleWidth, middleHeight + 100, this.Settings.output.hard, TextStyle.selectLevel.levels,
				() => this.startGame("hard"))
				.setColor("#f00");
		}

		// ------ Footer -> Back Game Button
		const backBTN = new TextButton(this, 50, height - 50, this.Settings.output.back, TextStyle.settings.back,
			() => this.returnHome())
			.setOrigin(0, 0.5);
	}

	startGame(level) {
		const data = {
			levelConfig: {
				...Configs.levels[level],
				levelName: level,
			},
		};
		this.scene.start("Game", data);
		this.scene.stop("Home");
		this.scene.stop();
	}

	returnHome() {
		EventManager.emit("ReactivateHomeButtons");
		this.scene.stop();
	}
}
