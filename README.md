# Super Box Jump

A simple game example of Phaser 3 Arcade

## Links
- [Play](https://201flaviosilva.gitlab.io/Super-Box-Jump);
- [Source Code](https://gitlab.com/201flaviosilva/Super-Box-Jump);

## Available Commands

| Command               | Description                                                                     |
| --------------------- | ------------------------------------------------------------------------------- |
| `npm install`         | Install project dependencies                                                    |
| `npm run clear`       | Delete the "build" and "out" and "dist" folder                                  |
| `npm start`           | Build project and open web server running project                               |
| `npm run build`       | Builds code bundle with production settings (minification, uglification, etc..) |
